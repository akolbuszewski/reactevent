import {categoriesFetched, eventsFetched, usersFetched} from "./actions";

export const fetchCategories = () =>
    (dispatch) => {
        fetch('http://www.mocky.io/v2/5bcdd3942f00002c00c855ba')
            .then((data) => data.json())
            .then(categories => dispatch(categoriesFetched(categories)));
    };

export const fetchUsers = () =>
    (dispatch) => {
        fetch('http://www.mocky.io/v2/5bcdd7992f00006300c855d5')
            .then((data) => data.json())
            .then(users => dispatch(usersFetched({
                users: [generateMyUser(users.length), ...users],
                myId: users.length,
            })));
    };

export const fetchEvents = () =>
    (dispatch) => {
        fetch('http://www.mocky.io/v2/5bcdd8732f00007300c855da')
            .then((data) => data.json())
            .then(events => dispatch(eventsFetched(events)));
    };

const generateMyUser = (usersLength) => ({
    id: usersLength,
    name: 'Aleksander',
    lastname: 'Kolbuszewski',
    email: 'aleksander.kolbuszewski@gmail.com'
});
