//Data loading
export const USERS_FETCHED = 'USERS_FETCHED';
export const CATEGORIES_FETCHED = 'CATEGORIES_FETCHED';
export const EVENTS_FETCHED = 'EVENTS_FETCHED';

//About
export const CHANGE_TITLE = 'CHANGE_TITLE';
export const CHANGE_DESCRIPTION = 'CHANGE_DESCRIPTION';
export const CHANGE_CATEGORY = 'CHANGE_CATEGORY';
export const TOGGLE_PAID_EVENT = 'TOGGLE_PAID_EVENT';
export const CHANGE_REWARD = 'CHANGE_REWARD';
export const CHANGE_FEE = 'CHANGE_FEE';

//Coordinator
export const CHANGE_COORDINATOR = 'CHANGE_COORDINATOR';
export const CHANGE_EMAIL = 'CHANGE_EMAIL';

//When
export const CHANGE_DATE ='CHANGE_DATE';
export const CHANGE_HOUR = 'CHANGE_HOUR';
export const CHANGE_DURATION = 'CHANGE_DURATION';

export const SUBMIT_FORM = 'SUBMIT_FORM';
export const VALIDATE_FORM = 'VALIDATE_FORM';

const createAction = (type) =>
     (payload) => ({
        type: type,
        payload,
    });

export const usersFetched = createAction(USERS_FETCHED);
export const categoriesFetched = createAction(CATEGORIES_FETCHED);
export const eventsFetched = createAction(EVENTS_FETCHED);


export const submitForm = createAction(SUBMIT_FORM);

//About actions
export const changeTitle = createAction(CHANGE_TITLE);
export const changeDescription = createAction(CHANGE_DESCRIPTION);
export const changeCategory = createAction(CHANGE_CATEGORY);
export const togglePaidEvent = createAction(TOGGLE_PAID_EVENT);
export const changeReward = createAction(CHANGE_REWARD);
export const changeFee = createAction(CHANGE_FEE);

export const changeCoordinator = createAction(CHANGE_COORDINATOR);
export const changeEmail = createAction(CHANGE_EMAIL);

export const changeDate = createAction(CHANGE_DATE);
export const changeHours = createAction(CHANGE_HOUR);
export const changeDuration = createAction(CHANGE_DURATION);

export const validateForm = createAction(VALIDATE_FORM);