import React, {Component} from 'react';
import './App.css';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {fetchCategories, fetchEvents, fetchUsers} from "./actions/thunks";
import EventForm from "./components/EventForm";


export class App extends Component {
    constructor(props) {
        super(props);
        this.fetchData();
    }

    fetchData = () => {
        this.props.fetchCategories();
        this.props.fetchUsers();
        this.props.fetchEvents();
    }

    render() {
        return (
            <EventForm/>
        );
    }
}


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchCategories,
        fetchUsers,
        fetchEvents,
    }, dispatch)
}
export default connect(null, mapDispatchToProps)(App);
