import React from "react";

export const SubmitSuccess = () =>
    <div className="succes-container">
        <h1>New event created</h1>
        <p>Event has been created</p>
    </div>

export default SubmitSuccess;