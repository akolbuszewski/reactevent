import React, {Component} from 'react';
import About from "./About";
import Cooordinator from "./Cooordinator";
import When from "./When";
import {selectAreFieldsValid, selectFormFields, selectIsTitleUnique} from "../store/selectors";
import {connect} from "react-redux";
import {Button, Form} from "./styled/shared";
import {bindActionCreators} from 'redux';
import {validateForm} from "../actions/actions";
import {push} from 'connected-react-router'


class EventForm extends Component {
    constructor(props) {
        super(props);
        this.formRef = React.createRef();

        document.addEventListener('invalid', (function () {
            return function (e) {
                //prevent the browser from showing default error bubble
                e.preventDefault();
                // optionally fire off some custom validation handler
                // myValidation();
            };
        })(), true);
    }

    render() {

        return (
            <Form id="form">
                <About/>
                <Cooordinator/>
                <When/>
                <div className="centerWrap">
                    <Button type="submit" form="form" value="PUBLISH EVENT"
                            onClick={() => this.submitForm()}>Submit</Button>
                </div>
            </Form>
        )
    }

    submitForm(event) {
        this.props.validateForm();
        if (this.props.isTitleUnique) {
            if (this.props.areFieldsValid) {
                this.props.push('/success');
                console.log({
                    ...this.props.formFields,
                    date: `${this.props.formFields.date.day}T${this.props.formFields.date.hour}`
                });
            }
        } else {
            alert(JSON.stringify('Event with that title exists. Please select different one'));
        }

    }
}


const mapStateToProps = state => ({
    isTitleUnique: selectIsTitleUnique(state),
    formFields: selectFormFields(state),
    areFieldsValid: selectAreFieldsValid(state),
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        validateForm,
        push,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EventForm);