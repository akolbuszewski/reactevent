import styled from "styled-components";

const size = {
    mobile: '546px',
    tablet: '768px',
    desktop: '976px',
    largeDesktop: '2000px',
};

const device = {
    mobile: `(max-width: ${size.mobile})`,
    tablet: `(max-width: ${size.tablet})`,
    desktop: `(max-width: ${size.desktop})`,
    largeDesktop: `(max-width: ${size.largeDesktop})`,
}

const inputColors = `
    &:invalid:focus{
    border-color: red;
}
    &:valid:focus{
    border-color: green;
}`;

const inputsStyling = `
    border-radius: 3px;
    border-color: #d3d3d3;
    border-width: 2px;
    border-style: solid;
`
export const Input = styled.input`
    ${inputsStyling};
    ${inputColors};
    height: 1.2rem;
`;

export const Select = styled.select`
    ${inputsStyling};
    height: 1.5rem;

`

export const Textarea = styled.textarea`
    ${inputsStyling};
    ${inputColors};
    height: 140px;
    resize: none;
    ${inputColors};
`

export const Label = styled.label`
    color: #56adff;
    @media ${device.desktop} { 
        margin-bottom:3px;
   }
`;

export const Fieldset = styled.fieldset`
  margin: 1rem 4rem 1rem 4rem;
  background-color: white;
  @media ${device.tablet} { 
    margin: 0.5rem 2rem 0.5rem 2rem;
   }
  @media ${device.mobile} { 
    margin: 0 1rem 0 1rem;
    }
`

export const GridRow = styled.div`
  display: grid;
  margin: 1rem;
  grid-template-columns: 20% 40% 30%;
  @media ${device.desktop} { 
    grid-template-columns: 25% 40% 30%; 
   }
  @media ${device.mobile} { 
    grid-template-columns: 85%;
    }
`;


export const Header = styled.header`
  font-size: 1.2rem;
  font-weight: 400;
  color: white;
  background: #39598C;
  height: 3rem;
  padding: 1rem 0 0 3rem;
`;

export const Button = styled.button`
  background-color:#FE8D36;
  border-radius: 2px;
  border: none;
  color: white;
  height: 30px;
  width: 90px;
  margin: 0.5rem;
`;

export const ElementDescription = styled.div`
  color: #d3d3d3;
  font-size: 0.6rem;
  font-style: italic;
`

export const FlexSpaceBetween = styled.div`
 display: flex;
 justify-content: space-between;
`
export const Flex = styled.div`
 display: flex;

`
export const InheritDiv = styled.div`
  display: inherit;
`

export const Warning = styled.div`
   background-color: #FEB1B1;
   color: white;
   border-radius: 5px;
   text-align: center;
   display:table;
   padding: 4px;
   margin-left: 3px;
   font-size: 0.8rem;
   @media ${device.mobile} { 
    margin-top: 3px;
    margin-left: 0px;
    }
`

export const Form = styled.form`
    width: 75%;
    @media ${device.desktop} { 
    width:85%;
   }
  @media ${device.tablet} { 
    width:100%;
   }
`

export const HourWrapper = styled.div`
  display: inline-block;
  margin-top: 0.3rem;
`

