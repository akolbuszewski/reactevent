import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {
    changeCategory,
    changeDescription,
    changeFee,
    changeReward,
    changeTitle,
    togglePaidEvent
} from "../actions/actions";
import {
    selectCategories,
    selectDescriptionLength, selectDescriptionValidity,
    selectIsPaid,
    selectTitleValidity,
    selectFeeValidity
} from "../store/selectors";
import {
    ElementDescription,
    Fieldset,
    Input,
    Label,
    GridRow,
    Textarea,
    FlexSpaceBetween,
    InheritDiv,
    Warning,
    Select,
} from "./styled/shared";
import {preventTypingNegative} from '../helpers/functions'

class About extends Component {

    render() {
        return (
            <Fieldset>
                <div className="fieldset-title">About</div>
                <GridRow>
                    <Label className="required-field" htmlFor="title">TITLE</Label>
                    <Input ref={elem => this.title = elem} onChange={(event) =>
                        this.props.changeTitle({value: event.target.value, validity: this.title.checkValidity()})}
                           type="text"
                           placeholder="Make it short and clear" name="title" id="title" required/>

                    {!this.props.titleValidity ?
                        <Warning>Title cannot be empty</Warning> : null}
                </GridRow>
                <GridRow>
                    <Label className="required-field" htmlFor="title">DESCRIPTION</Label>
                    <InheritDiv>
                        <Textarea ref={elem => this.description = elem}
                                  onChange={(event) => this.props.changeDescription({
                                      value: event.target.value,
                                      validity: this.description.checkValidity()
                                  })}
                                  name="description" maxLength="140"
                                  placeholder="Write about your event, be creative" required></Textarea>
                        <FlexSpaceBetween>
                            <ElementDescription>Max length 140 characters</ElementDescription>
                            <ElementDescription>{`${this.props.descriptionLength} of 140`}</ElementDescription>
                        </FlexSpaceBetween>
                    </InheritDiv>
                    {!this.props.descriptionValidity ?
                        <Warning>Description cannot be empty</Warning> : null}
                </GridRow>

                <GridRow>
                    <Label htmlFor="categories">CATEGORIES</Label>
                    <InheritDiv>
                        <Select id="categories" onChange={(event) => this.props.changeCategory(event.target.value)}
                                required>
                            {this.mapCategoriesToOptions()}
                        </Select>
                        <ElementDescription>Describes topic and people who should be interested in this
                            event</ElementDescription>
                    </InheritDiv>
                </GridRow>

                <GridRow>
                    <Label htmlFor="payment">PAYMENT</Label>
                    <div>
                        <input type="radio" id="free"
                               name="payment" onChange={() => this.props.togglePaidEvent(false)} required/>
                        <label className="radio-label" htmlFor="contactChoice1">Free</label>
                        <input type="radio" id="paid"
                               name="payment" onChange={() => this.props.togglePaidEvent(true)}/>
                        <label className="radio-label" htmlFor="contactChoice2">Paid</label>
                        {this.props.isPaid ?
                            <Input className="small-input" ref={elem => this.fee = elem} type="number" required
                                   placeholder="Fee" min="0" id="fee"
                                   onChange={(event) => this.props.changeFee({
                                       value: event.target.value,
                                       validity: this.fee.checkValidity()
                                   })}/> :
                            null}
                    </div>
                    {!this.props.feeValidity ?
                        <Warning>Fee cannot be empty</Warning> : null}
                </GridRow>

                <GridRow>
                    <Label htmlFor="reward">REWARD</Label>
                    <Input className="small-input" type="number"
                           onChange={(event) => this.props.changeReward(event.target.value)}
                           placeholder="Number" name="reward" id="reward" min="0"/>
                </GridRow>

            </Fieldset>
        )
    }

    componentDidMount() {
        const reward = document.getElementById('reward');
        if (reward) {
            reward.onkeydown = preventTypingNegative;
        }
    }

    componentDidUpdate() {
        const fee = document.getElementById('fee');
        if (fee) {
            fee.onkeydown = preventTypingNegative;
        }
    }

    mapCategoriesToOptions() {
        return this.props.categories.map(
            category =>
                <option key={category.name.toLowerCase()} value={category.id}>{category.name}</option>
        )
    }
}

const mapStateToProps = state => ({
    categories: selectCategories(state),
    descriptionLength: selectDescriptionLength(state),
    isPaid: selectIsPaid(state),
    titleValidity: selectTitleValidity(state),
    descriptionValidity: selectDescriptionValidity(state),
    feeValidity: selectFeeValidity(state),
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        changeTitle,
        changeDescription,
        changeCategory,
        togglePaidEvent,
        changeReward,
        changeFee,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(About);


