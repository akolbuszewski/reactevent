import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {changeDate, changeDuration, changeHours} from "../actions/actions";
import {Fieldset, Label, GridRow, Warning, Input, HourWrapper} from "./styled/shared";
import {selectDateValidity} from "../store/selectors";
import {preventTypingNegative} from '../helpers/functions'


class When extends Component {

    render() {
        return (
            <Fieldset>
                <div className="fieldset-title">When</div>
                <GridRow>
                    <Label className="required-field" htmlFor="date">DATE</Label>
                    <div>
                        <Input type="date" onChange={(event) => this.props.changeDate(event.target.value)} id="date"
                               name="date" min={new Date().toISOString().substring(0, 10)} required/>
                        <HourWrapper className="hour-wrapper">
                            <span className="date-divider">at</span>
                            <Input type="time" onChange={(event) => this.props.changeHours(event.target.value)}
                                   id="appt"
                                   name="appt"
                                   required/>
                        </HourWrapper>
                    </div>
                    {!this.props.dateValidity ?
                        <Warning>Date cannot be empty</Warning> : null}
                </GridRow>
                <GridRow>
                    <Label htmlFor="duration">DURATION</Label>
                    <Input className="small-input" type="number"
                           onChange={(event) => this.props.changeDuration(event.target.value)}
                           id="duration" name="duration" min="0" placeholder="Hours"/>
                </GridRow>
            </Fieldset>
        )
    }

    componentDidMount() {
        const duration = document.getElementById('duration');
        duration.onkeydown = preventTypingNegative;
        const date = document.getElementById('date');
        date.onkeydown = () => false;
    }

}

const mapStateToProps = state => ({
    dateValidity: selectDateValidity(state),
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        changeDate,
        changeHours,
        changeDuration,

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(When);