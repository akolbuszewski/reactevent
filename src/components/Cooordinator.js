import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    changeCoordinator,
    changeEmail,
} from "../actions/actions";
import {bindActionCreators} from 'redux';
import {selectEmailValidity, selectMyUser, selectOtherUsers} from "../store/selectors";
import {Fieldset, Label, GridRow, Input, Warning, Select} from "./styled/shared";

class Cooordinator extends Component {

    render() {
        return (
            <Fieldset>
                <div className="fieldset-title">Coordinator</div>
                <GridRow>
                    <Label className="required-field" htmlFor="responsible">RESPONSIBLE</Label>
                    <Select id="responsible" onChange={(event) => this.props.changeCoordinator(event.target.value)}
                            required>
                        {this.mapMyUserToOptions()}
                        {this.mapOtherUsersToOptions()}
                    </Select>
                </GridRow>
                <GridRow>
                    <Label htmlFor="email">EMAIL</Label>
                    <Input ref={elem => this.email = elem} type="email"
                           onChange={(event) => this.props.changeEmail({
                               value: event.target.value,
                               validity: this.email.checkValidity()
                           })}
                           name="email" placeholder="Email" size="30" id="email" required/>
                    {!this.props.isEmailValid ?
                        <Warning>Wrong email format</Warning> : null}
                </GridRow>
            </Fieldset>
        )
    }

    mapMyUserToOptions() {
        const {myUser} = this.props;
        if (!myUser) {
            return;
        }
        return (
            <optgroup label="Me">
                <option key={myUser.id}
                        value={myUser.id}>{`${myUser.name} ${myUser.lastname}`
                }
                </option>
            </optgroup>)
    }


    mapOtherUsersToOptions() {
        const {otherUsers} = this.props;
        return (
            <optgroup label="Others">{
                otherUsers.map(
                    user =>
                        <option key={user.name.toLowerCase()}
                                value={user.id}>{`${user.name} ${user.lastname}`
                        }</option>
                )
            }  </optgroup>)
    }


}

const mapStateToProps = state => ({
    myUser: selectMyUser(state),
    otherUsers: selectOtherUsers(state),
    isEmailValid: selectEmailValidity(state),
});

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        changeCoordinator,
        changeEmail,
    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(Cooordinator);
