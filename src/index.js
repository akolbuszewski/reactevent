import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './styles.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import configureStore from "./store/store";
import {Provider} from "react-redux";
import {ConnectedRouter} from 'connected-react-router'
import {history} from './reducers/rootReducer'
import {Route, Switch} from "react-router";
import SubmitSuccess from "./components/SubmitSuccess";
import {Header} from "./components/styled/shared";

ReactDOM.render(
    <Provider store={configureStore()}>
        <Header>
            New event
        </Header>
        <ConnectedRouter history={history}>
            <Switch>
                <Route exact path="/" render={() => <App/>}/>
                <Route exact path="/success" render={() => <SubmitSuccess/>}/>
                <Route render={() => (<div>Wrong URL</div>)}/>
            </Switch>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
