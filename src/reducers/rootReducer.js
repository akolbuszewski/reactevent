import { combineReducers } from 'redux'
import dataLoadingReducer from './dataLoadingReducer'
import formReducer from "./formReducer";
import { connectRouter } from 'connected-react-router'
import {createBrowserHistory} from "history";

export const history = createBrowserHistory();

export default combineReducers({
    data: dataLoadingReducer,
    form: formReducer,
    router: connectRouter(history),
})