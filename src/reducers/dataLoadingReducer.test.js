import { usersFetched, categoriesFetched, eventsFetched } from "../actions/actions";
import { defaultState } from "./dataLoadingReducer";
import dataLoadingReducer from "./dataLoadingReducer";
describe('Data loding reducer', ()=>{

    it('should add users to state', ()=>{
        const users = [{id:1, name:'olek', lastname:'olkowski', email:'email@test.pl'},
                        {id:2, name: 'artur', lastname:'arturowski', email:'email1@test.pl'},
                        {id:3, name: 'michal', lastname:'michalowski', email:'email2@test.pl'}
                    ];
        const action = usersFetched({
            users,
            myId: 2,
        });

        Object.freeze(action);
        Object.freeze(defaultState);

        const obtainedState = dataLoadingReducer(defaultState, action);
        expect(obtainedState).toEqual({...defaultState, users, myId: 2});
    })

    it('should add categories to state', ()=>{
        const categories = [{id:1, name:'swimming'},
                        {id:2, name: 'golf'},
                        {id:3, name: 'boxing'}
                    ];
        const action = categoriesFetched(categories);

        Object.freeze(action);
        Object.freeze(defaultState);

        const obtainedState = dataLoadingReducer(defaultState, action);
        expect(obtainedState).toEqual({...defaultState, categories});
    })

    it('should add events to state', ()=>{
        const events = [
            {userId: 1,id: 1,title: "Test title"},
            {userId: 1,id: 2,title: "Some title"},
            {userId: 6,id: 7,title: "My title"},
        ]
        const action = eventsFetched(events);

        Object.freeze(action);
        Object.freeze(defaultState);

        const obtainedState = dataLoadingReducer(defaultState, action);
        expect(obtainedState).toEqual({...defaultState, events});
    })
})