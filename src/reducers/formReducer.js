import {
    CHANGE_CATEGORY, CHANGE_COORDINATOR, CHANGE_DATE,
    CHANGE_DESCRIPTION, CHANGE_DURATION, CHANGE_EMAIL, CHANGE_FEE, CHANGE_HOUR,
    CHANGE_REWARD,
    CHANGE_TITLE,

    TOGGLE_PAID_EVENT, VALIDATE_FORM, USERS_FETCHED, CATEGORIES_FETCHED
} from "../actions/actions";
import { get } from 'lodash';

const defaultState = {
    validity: {
        title: false,
        description: false,
        date: false,
        email: true,
        fee: true,
    },
    fields: {},
    highlightWarnings: false,
}

export default (state = defaultState, action) => {
    switch (action.type) {
        case CHANGE_TITLE :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    title: action.payload.validity,
                },
                fields: {
                    ...state.fields,
                    title: action.payload.value,
                }
            };
        case CHANGE_DESCRIPTION :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    description: action.payload.validity,
                },
                fields: {
                    ...state.fields,
                    description: action.payload.value,
                }
            };
        case CHANGE_CATEGORY :
            return {
                ...state,
                fields: {
                    ...state.fields,
                    category_id: parseInt(action.payload),
                }
            };
        case TOGGLE_PAID_EVENT :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    event_fee: !action.payload
                },
                fields: {
                    ...state.fields,
                    paid_event: action.payload,
                    event_fee: action.payload ? state.fields.fee : undefined,
                }
            };
        case CHANGE_FEE :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    event_fee: action.payload.validity,
                },
                fields: {
                    ...state.fields,
                    event_fee: parseInt(action.payload.value),
                }
            };
        case CHANGE_REWARD :
            return {
                ...state,
                fields: {
                    ...state.fields,
                    reward: action.payload,
                }
            };
        case CHANGE_COORDINATOR :
            return {
                ...state,
                fields: {
                    ...state.fields,
                    coordinator: {
                        ...state.fields.coordinator,
                        id: action.payload,
                    }
                }
            };
        case CHANGE_EMAIL :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    email: action.payload.value ? action.payload.validity : true,
                },
                fields: {
                    ...state.fields,
                    coordinator: {
                        ...state.fields.coordinator,
                        email: action.payload.value,
                    }
                }
            };
        case CHANGE_DATE :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    date: !!get(state,'fields.date.hour'),
                },
                fields: {
                    ...state.fields,
                    date:{
                        ...state.fields.date,
                        day:action.payload},
                }
            };
        case CHANGE_HOUR :
            return {
                ...state,
                validity: {
                    ...state.validity,
                    date: !!get(state,'fields.date.day'),
                },
                fields: {
                    ...state.fields,
                    date:{
                        ...state.fields.date,
                        hour:action.payload},
                }
            };
        case CHANGE_DURATION :
            return {
                ...state,
                fields: {
                    ...state.fields,
                    duration: parseInt(action.payload),
                }
            };
        case VALIDATE_FORM :
            return {
                ...state,
                highlightWarnings: true,
            };
        case USERS_FETCHED: 
        return {
            ...state,
            fields: {
                ...state.fields,
                coordinator: {
                    ...state.fields.coordinator,
                    id: action.payload.myId,
                }
            }
        };
        case CATEGORIES_FETCHED:
            return  {
            ...state,
            fields: {
                ...state.fields,
                category_id: get(action.payload,'[0].id'),
            }
        };
        default:
            return state;
    }
}