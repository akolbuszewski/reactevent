import {CATEGORIES_FETCHED, EVENTS_FETCHED, USERS_FETCHED} from "../actions/actions";

export const defaultState = {
    users: [],
    categories: [],
    events: [],
}


export default (state = defaultState, action) => {
    switch(action.type){
        case USERS_FETCHED :
            return{
                ...state,
                users: action.payload.users,
                myId: action.payload.myId,
            }
        case CATEGORIES_FETCHED : {
            return{
                ...state,
                categories: action.payload,
            }
        }
        case EVENTS_FETCHED : {
            return{
                ...state,
                events: action.payload,
            }
        }
        default:
            return state;
    }
}