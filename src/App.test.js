import React from 'react';
import ReactDOM from 'react-dom';
import {App} from './App';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const mockFun = ()=> {};

  // TODO mock EVENT FORM but how to use Jest from CRA?
  // mock("./components/EventForm", () => ()=> <div id="mockEventForm">
  //   mockUserCom
  // </div>)


  ReactDOM.render(<App fetchCategories={mockFun} fetchUsers={mockFun} fetchEvents={mockFun}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
