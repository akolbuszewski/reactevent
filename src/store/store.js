import rootReducer from "../reducers/rootReducer";
import {createStore,compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from "redux-devtools-extension";
import { routerMiddleware } from 'connected-react-router'
import { history } from './../reducers/rootReducer'

export default function configureStore() {
    return createStore(
        rootReducer,
        compose(
            applyMiddleware(thunk, routerMiddleware(history))
            // composeWithDevTools()
             ),

    );
}
