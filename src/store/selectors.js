import { createSelector } from "reselect";

export const selectData = state => state.data;
export const selectForm = state => state.form;

export const selectFormFields = state => selectForm(state).fields;
export const selectFormValidity = state => selectForm(state).validity;
export const selectFormHighlightWarnings = state =>
  selectForm(state).highlightWarnings;
export const selectUsers = state => selectData(state).users;
export const selectMyId = state => selectData(state).myId;
export const selectCategories = state => selectData(state).categories;
export const selectEvents = state => selectData(state).events;

export const selectMyUser = createSelector(
  [selectUsers, selectMyId],
  (users, myId) => (users ? users.find(user => user.id === myId) : undefined)
);

export const selectOtherUsers = state => {
  const users = selectUsers(state);
  const myId = selectMyId(state);
  return users ? users.filter(user => user.id !== myId) : undefined;
};

export const selectIsTitleUnique = state => {
  const { title } = selectFormFields(state);
  const events = selectEvents(state);
  return !events.find(event => event.title === title);
};

export const selectDescriptionLength = state => {
  const { description } = selectFormFields(state);
  return description ? description.length : 0;
};

export const selectTitleValidity = createSelector(
  [selectFormValidity, selectFormHighlightWarnings],
  (validity, highlight) => (highlight ? (validity.title ? true : false) : true)
);

export const selectDescriptionValidity = createSelector(
  [selectFormValidity, selectFormHighlightWarnings],
  [selectFormValidity, selectFormHighlightWarnings],
  (validity, highlight) =>
    highlight ? (validity.description ? true : false) : true
);

export const selectDateValidity = createSelector(
  [selectFormValidity, selectFormHighlightWarnings],
  (validity, highlight) => (highlight ? (validity.date ? true : false) : true)
);

export const selectEmailValidity = createSelector(
  [selectFormValidity, selectFormHighlightWarnings],
  (validity, highlight) => (highlight ? (validity.email ? true : false) : true)
);

export const selectFeeValidity = createSelector(
  [selectFormValidity, selectFormHighlightWarnings],
  (validity, highlight) => (highlight ? (validity.event_fee ? true : false) : true)
);

export const selectAreFieldsValid = createSelector(
  selectFormValidity,
  validity => Object.values(validity).reduce((acc, elem) => acc && elem, true)
);

export const selectIsPaid = state => selectFormFields(state).paid_event;
